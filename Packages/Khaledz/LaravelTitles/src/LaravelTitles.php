<?php

namespace Khaledz\LaravelTitles;

use Route;
use Exception\FileNotFoundException;

class LaravelTitles
{
	/**
     * Illuminate\Routing\Router
     *
     * @var \Illuminate\Routing\Router
     */
    protected $route;
    
    /**
     * The request of current HTTP.
     *
     * @var \Illuminate\Http\Request $request
     */
	protected $request;

	/**
     * Create a new laravel title instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
	public function __construct($request)
	{
        $this->request = $request;
        $this->route = Route::getCurrentRoute()->getAction();
    }
    
	/**
     * Get the title by the language of the app.
     *
     * @return string
     */
	public function get()
	{
        if(! $this->isTransAvailable())
        {
            throw new \Exception("Cannot find the key {$this->getAction()} of controller {$this->getController()} in title file.");
        }

        // if(! $this->isControllerAvailable())
        // {
        //     return 'no-title';
        // }

        return trans("title.{$this->getController()}.{$this->getAction()}");
	}

	/**
     * Get the controler of the current route.
     *
     * @return string
     */
	protected function getController()
	{
        if(isset($this->route['controller']))
		return explode('@', $this->route['controller'])[0];
	}

	/**
     * Get the action of the current route.
     *
     * @return string
     */
	protected function getAction()
	{
        if(isset($this->route['controller']))
		return explode('@', $this->route['controller'])[1];
	}

    /**
     * check if the title is available to fetch.
     *
     * @return boolean
     */
    private function isTransAvailable()
    {
        if(! $this->isControllerAvailable())
        {
            return false;
        }

        $data = $this->tryLoadTranslation();
        
        return array_key_exists($this->getAction(), $data[$this->getController()]);
    }

    private function tryLoadTranslation()
    {
        try
        {
            $data = include(resource_path('lang/'. app()->getLocale() .'/title.php'));
        }
        catch(FileNotFoundException $e)
        {
            return $e->getMessage();
        }

        return $data;
    }

    /**
     * check if the controller is available to get.
     *
     * @return boolean
     */
     private function isControllerAvailable()
     {
         return $this->route['controller'] != null;
     }

    /**
     * Get the last string of url.
     *
     * @return string
     */
    private function getSlug()
    {
        return str_replace(["-", "–"], ' ', array_slice(explode('/', rtrim($this->request->getRequestUri(), '/')), -1)[0]);
    }

    /**
     * Get the last string of url.
     *
     * @return string
     */
    private function getName()
    {
        return str_replace("%20", " ", array_slice(explode('/', rtrim($this->request->getRequestUri(), '/')), -1)[0]);
    }

    
}