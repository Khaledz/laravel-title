<?php

namespace Khaledz\LaravelTitles\Middleware;

use Closure;
use Khaledz\LaravelTitles\LaravelTitles;
use View;

class LaravelTitlesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        View::share('title', (new LaravelTitles($request))->get());

        return $next($request);
    }
}
