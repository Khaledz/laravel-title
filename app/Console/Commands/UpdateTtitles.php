<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateTtitles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'title:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh the title files.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
